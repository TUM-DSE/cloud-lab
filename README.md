# Lab: Cloud Systems Engineering  (“cloud-lab”) -- SoSe 2022

## Chair website

- The lab is organized by the [Chair of Decentralized Systems Engineering](https://dse.in.tum.de/) at TU Munich.

## Registration info

- **Capacity:** We plan to have at most 48 places in this course.
- **Registration deadline:** Two weeks after the matching period, we will formally register you in the course. If you want to drop out, please inform us before the registration deadline.  

## Slack workspace

We will use Slack for all communication. Please enroll in our Slack workspace using your official TUM email address.

- **Slack workspace:** https://ls1-courses-tum.slack.com
- **Slack channel:** #ss-22-cloud-lab

## Meeting place

We will be meeting at the following location for Q&A sessions:

- Join Zoom Meeting: https://tum-conf.zoom.us/j/68516837406 (Passcode 482722)

## Schedule and material


- **Lecture videos playlist** (released on Mondays before Q&As): [YouTube playlist](https://www.youtube.com/watch?v=JPUf37_hB5g&list=PLfKm1-FQibbAKAx6fji1YE6eKsd79RrP-)



|         Topic and slides                       |  Q&A session (**Thursdays from 10-11am**)            |    Task duration           |
|------------------------------------------------|--------------------------|----------------------------|
| [Kick-off meeting](docs/kick-off.pdf)                      |  28th April |  N/A |       
| [Task #0: Containers and job deployment](docs/task-0-containers.pdf)|  5th May | 2 weeks |
| [Task #1: Single-node KVS](docs/task-1-rocksDB.pdf)              |  19th May  |  3 weeks |               
| [Task #2: Distributed KVS](docs/)              |  9th June  | 3 weeks |            
| [Task #3: Fault-tolerant KVS](docs/)           |  30th June | 3 weeks | 



## Contact

We *strongly* prefer slack for all communications. For any further questions/comments, please contact the course organizer(s):
  - [Prof. Bhatotia](https://dse.in.tum.de/bhatotia/)
  - [Emmanouil (Manos) Giortamis](https://dse.in.tum.de/manos-giortamis/)

